require_relative './colocator/term_indexer'
require_relative './colocator/index_comparer'
require_relative './colocator/colocation_aggregator'

class Colocator
  attr_reader :file_path, :first_term, :second_term, :wordcount_between_terms
  attr_reader :first_term_location_indices, :second_term_location_indices, :term_colocation_count

  def initialize (file_path:, first_term:, second_term:, wordcount_between_terms:)
    @file_path = file_path
    @first_term = first_term
    @second_term = second_term
    @wordcount_between_terms = wordcount_between_terms

    @first_term_location_indices = []
    @second_term_location_indices = []

    process_search_terms()
    process_term_indices_comparison()
  end

  private
  def process_search_terms
    @first_term_location_indices = TermIndexer.indices_by_term(file_path: @file_path, term_to_index: @first_term)
    @second_term_location_indices = TermIndexer.indices_by_term(file_path: @file_path, term_to_index: @second_term)
  end

  private
  def process_term_indices_comparison
    @term_colocation_count = IndexComparer.matches_by_index_proximity(
      first_indices: @first_term_location_indices,
      second_indices: @second_term_location_indices,
      minimum_proximity: @wordcount_between_terms
    )
  end
end
