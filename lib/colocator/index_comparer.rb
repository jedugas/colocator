class IndexComparer
  def self.matches_by_index_proximity(first_indices:, second_indices:, minimum_proximity:)
    matches_by_proximity = 0
    # count the number of times the indices are < N words away from eachother
    first_indices.each { |target_index|
      second_indices.each { |compare_index|
        if target_index.between?(compare_index - minimum_proximity, compare_index + minimum_proximity)
          matches_by_proximity += 1
        end
      }
    }
    return matches_by_proximity
  end
end
