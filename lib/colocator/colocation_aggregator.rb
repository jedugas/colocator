class ColocationAggregator
  attr_reader :directory_path, :first_term, :second_term, :wordcount_between_terms
  attr_reader :colocation_results

  def initialize(directory_path:, first_term:, second_term:, wordcount_between_terms:)
    @directory_path = directory_path
    @first_term = first_term
    @second_term = second_term
    @wordcount_between_terms = wordcount_between_terms

    @colocation_results = nil
    generate_colocation_results()
  end

  def display_colocation_results()
    #puts JSON.pretty_generate(indexer_results)
    puts "Terms: '#{@first_term}' and '#{@second_term}' | Term Proximity: #{@wordcount_between_terms}"
    @colocation_results.each do |key, value|
      puts "File: #{key} | Terms are co-located: #{value} times based on term proximity"
    end
  end

  private
  def generate_colocation_results()
    @colocation_results = Hash.new()
    target_files = Dir.entries(@directory_path).select { |item_in_directory| !File.directory? item_in_directory }
    target_files.each { |file_name|
      file_path = File.join(@directory_path, file_name)
      colocator = Colocator.new(file_path: file_path,first_term: @first_term, second_term: @second_term, wordcount_between_terms: @wordcount_between_terms)
      @colocation_results[file_name] = colocator.term_colocation_count
    }
  end
end
