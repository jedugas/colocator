class TermIndexer
  def self.indices_by_term(file_path:, term_to_index:)
    term_found_indices = Array.new()
    file_content = IO.read(file_path).downcase()
    replacement_term = nil

    if term_to_index.include?(" ")
      replacement_term = '_' * term_to_index.length
      file_content = file_content.gsub(term_to_index, replacement_term)
    end

    # split the document into words while removing punctuation
    file_words = file_content.split(/\W+/)
    (0..file_words.length).each do |word_index|
      if file_words[word_index] == (replacement_term || term_to_index)
        term_found_indices.push(word_index)
      end
    end
    return term_found_indices
  end
end
