Vagrant.configure(2) do |config|
  ENV['VAGRANT_DEFAULT_PROVIDER'] = "virtualbox"

  config.vm.communicator = "ssh"
  config.vm.box = "box-cutter/ubuntu1404-desktop"
  config.vm.network "private_network", type: 'dhcp' # ip: "192.168.33.10"
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = "4096"
  end
  config.vm.provision "shell", inline: <<-CURL
    sudo apt-get update
    sudo apt-get install curl -y
  CURL

  config.vm.provision "shell", privileged: false, inline: <<-ATOM
    sudo add-apt-repository ppa:webupd8team/atom -y
    sudo apt-get update
    sudo apt-get install atom -y
  ATOM

  config.vm.provision "shell", privileged: false, inline: <<-RVM
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    curl -sSL https://get.rvm.io | bash -s stable
  RVM

  config.vm.provision "shell", privileged: false, inline: <<-RUBY
    #!/usr/bin/env bash
    source $HOME/.rvm/scripts/rvm
    rvm use --default --install 2.2.1
    shift
    if (( $# ))
    then gem install $@
    fi
    rvm cleanup all
  RUBY

  config.vm.provision "shell", privileged: false, inline: <<-LAUNCHER
    favs="atom.desktop gnome-terminal.desktop"
    for fav in $favs
    do
      list=`sudo -H -u vagrant dbus-launch gsettings get com.canonical.Unity.Launcher favorites`
      newlist=`echo $list | sed s/]/", '${fav}']"/`
      sudo -H -u vagrant dbus-launch gsettings set com.canonical.Unity.Launcher favorites "$newlist"
    done
    sudo service lightdm restart
  LAUNCHER

  config.vm.provision "shell", privileged: false, inline: <<-BASHRC
    echo 'if test -f ~/.rvm/scripts/rvm; then' >> ~/.bashrc
    echo '    [ "$(type -t rvm)" = "function" ] || source ~/.rvm/scripts/rvm' >> ~/.bashrc
    echo 'fi' >> ~/.bashrc
  BASHRC

  config.vm.provision "shell", privileged: false, inline: <<-KITCHEN
    rvm use  2.2.1@kitchen_gemset --create
    gem install test-kitchen
    gem install rspec
    gem-install bundler
  KITCHEN

  #config.vm.provision :shell, path: "install-rvm.sh", args: "stable", privileged: false
  #config.vm.provision :shell, path: "install-ruby.sh", args: "2.2.1", privileged: false
end
