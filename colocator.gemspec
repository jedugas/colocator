# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "colocator"
  spec.version       = "0.1.0"
  spec.authors       = ["Jeffrey Dugas"]
  spec.email         = ["jedugas@gmail.com"]

  spec.summary       = %q{Discovers the co-locality of two search terms or phrases.}
  spec.description   = %q{For each .txt file in the target folder, discovers the number of times two terms or phrases are co-located.}
  spec.homepage      = "https://bitbucket.org/jedugas/colocator"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = [
    'lib/colocator.rb',
    'lib/colocator/colocation_aggregator.rb',
    'lib/colocator/index_comparer.rb',
    'lib/colocator/term_indexer.rb'
  ]  #`git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  #spec.add_runtime_dependency 'json', '~> 0'
  #spec.add_dependency 'json'

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.4"
  spec.add_development_dependency "rspec", "~> 3.4"
end
