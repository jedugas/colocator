require_relative './../spec_helper'

describe Colocator do

  before :all do
    @sample_data_file = File.expand_path('../data/doc_2.txt', __FILE__)
    puts "Sample data file @ " + @sample_data_file

    @sample_first_term = 'yet'
    @sample_second_term = 'indeed'
    @sample_spacing = 10
    @colocator = Colocator.new(
      file_path: @sample_data_file,
      first_term: @sample_first_term,
      second_term: @sample_second_term,
      wordcount_between_terms: @sample_spacing
    )
  end

  it 'accepts four parameters for initialization' do
    expect(@colocator).to( be_an_instance_of( Colocator ) )
  end

  it 'exposes its properties as readable attributes' do
    expect(@colocator.file_path).to( be( @sample_data_file ) )
    expect(@colocator.first_term).to( be( @sample_first_term ) )
    expect(@colocator.second_term).to( be( @sample_second_term ) )
    expect(@colocator.wordcount_between_terms).to( be( @sample_spacing ) )
  end

  it 'can tell me how many times the first_term is found in the file' do
    expect(@colocator.first_term_location_indices).to( eq( [40, 177, 228, 456, 472] ) )
  end

  it 'can tell me how many times the second_term is found in the file' do
    expect(@colocator.second_term_location_indices).to( eq( [466] ) )
  end

  it 'can tell me how often the two terms are co-located' do
    expect(@colocator.term_colocation_count).to( eq( 2 ) )
  end

end
