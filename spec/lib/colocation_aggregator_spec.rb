require_relative './../spec_helper'

describe ColocationAggregator do

  before :all do
    @sample_folder = File.expand_path('../data', __FILE__)
    puts "Sample data folder @ " + @sample_folder

    @sample_first_term = 'yet'
    @sample_second_term = 'indeed'
    @sample_spacing = 10
    @aggregator = ColocationAggregator.new(
      directory_path: @sample_folder,
      first_term: @sample_first_term,
      second_term: @sample_second_term,
      wordcount_between_terms: @sample_spacing
    )
  end

  it 'accepts four parameters for initialization' do
    expect(@aggregator).to( be_an_instance_of( ColocationAggregator ) )
  end

  it 'exposes its properties as readable attributes' do
    expect(@aggregator.directory_path).to( be( @sample_folder ) )
    expect(@aggregator.first_term).to( be( @sample_first_term ) )
    expect(@aggregator.second_term).to( be( @sample_second_term ) )
    expect(@aggregator.wordcount_between_terms).to( be( @sample_spacing ) )
  end

  it "gives the expected co-location results for 'yet' and 'indeed' with a distance of 10 words apart" do
    expect(@aggregator.colocation_results).to( eq( { "doc_1.txt" => 0, "doc_2.txt" => 2, "doc_3.txt" => 0 } ) )
  end

  it "gives the expected co-location results for 'yet' and 'common indeed' with a distance of 9 words apart" do
    new_aggregator = ColocationAggregator.new(
      directory_path: @sample_folder,
      first_term: @sample_first_term,
      second_term: 'common indeed',
      wordcount_between_terms: 9
    )
    expect(new_aggregator.colocation_results).to( eq( { "doc_1.txt" => 0, "doc_2.txt" => 2, "doc_3.txt" => 0 } ) )
  end
end
